var scriptUpdater = {
    TWDW: '0.3.0',
    TWDWNew: 'New feature: Watched Players',
    TWCAO: '0.0.3',
    TWCAONew: 'Added a dialog which asks whether you want to open the alliance chat or not.',
    TWDCC: '0.0.1-beta-2',
    TWDCCNew: 'Update link added.'
};